<? if (empty($arResult['IS_AJAX'])) { ?>
    <div class="<?= $arResult['WRAPPER']['CONTANER'] ?>">
    <? } ?>
    <? if ($arResult['IS_FILL']) { ?>
        <?
        $first = array_shift($arResult['MESSAGE']);
        $last = array_shift($arResult['MESSAGE']);
        ?>
        <div class="edit-game"><span class="edit-game-message" data-post="<?= $arParams['POST_ID'] ?>">ответить</span>
            <? if ($arResult["POST"] > 2) { ?>
                <span class="trigger">Все комментарии (<?= $arResult["POST"] ?>)</span>
            <? } ?>
        </div>
        <div class="wrap-icon comment-block<?= $first['LEVEL'] ?>">
            <a href="<?= $first['AUTHOR']['URL'] ?>">
                <div class="icon" style="background:url('<?= $first['AUTHOR']['PERSONAL_PHOTO_MEDIUM'] ?>');"></div>
                <div class="txt"><?= $first['AUTHOR']['NAME'] ?></div>
            </a>
        </div>
        <div class="message-body">
            <div class="content-block">
                <div class="time"><?= $first['DATE'] ?></div>
                <?= $first['POST_MESSAGE_TEXT'] ?>
            </div>
        </div>
        <div class="clear"></div>
        <div class="messages-block-wrapper">
            <? if ($last) { ?>
                <div class="comment-block comment-block<?= $last['LEVEL'] ?>">
                    <a href="<?= $last['AUTHOR']['URL'] ?>">
                        <div class="u-icon" style="background:url('<?= $last['AUTHOR']['PERSONAL_PHOTO_SMALL'] ?>');"><span class="you"><?= $last['AUTHOR']['NAME'] ?></span></div>
                    </a>
                    <div class="u-comment">
                        <div class="time"><?= $last['DATE'] ?></div>
                        <?= $last['POST_MESSAGE_TEXT'] ?>
                    </div>
                    <div class="clear"></div>
                </div>
            <? } ?>
            <div class="messages-block-wrapper-last">
                <div class="<?= $arResult['WRAPPER']["CONTANER_LIST"] ?>">
                <? } ?>
                <? foreach ($arResult['MESSAGE'] as $key => $arItem) { ?>
                    <div class="comment-block comment-block<?= $arItem['LEVEL'] ?>">
                        <a href="<?= $arItem['AUTHOR']['URL'] ?>">
                            <div class="u-icon" style="background:url('<?= $arItem['AUTHOR']['PERSONAL_PHOTO_SMALL'] ?>');"><span class="you"><?= $arItem['AUTHOR']['NAME'] ?></span></div>
                        </a>
                        <div class="u-comment">
                            <div class="time"><?= $first['DATE'] ?></div>
                            <?= $arItem['POST_MESSAGE_TEXT'] ?>
                        </div>
                        <div class="clear"></div>
                    </div>
                <? } ?>
                <? if ($arResult['IS_FILL']) { ?>
                </div>
                <? if ($arResult['PAGE_IS_NEXT']) { ?>
                    <div class="wrap_show-more">
                        <div class="show-more <?= $arResult['WRAPPER']["CONTANER_MORE"] ?>">Показать еще</div>
                    </div>
                <? } ?>
            </div>
        </div>
    <? } ?>
    <? if (empty($arResult['IS_AJAX'])) { ?>
    </div>
<? } ?>