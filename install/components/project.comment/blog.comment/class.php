<?php

use Bitrix\Main\Loader,
    Bitrix\Main\Page\Asset,
    Bitrix\Main\Application,
    Project\Core\Themes,
    Project\Comment\Post;

class ProjectCommentElement extends CBitrixComponent {

    private function startWrapperTemplate() {
        $this->arResult['AJAX'] = $this->GetPath() . '/ajax.php';
        $this->arResult['SCRIPT'] = $this->GetPath() . '/script.js';
        Asset::getInstance()->addJs($this->arResult['SCRIPT']);
        Themes::addJs('js/loader.js');
        $jsParams = array(
            'AJAX' => $this->arResult['AJAX'],
            'TEMPLATE_NAME' => $this->arResult['TEMPLATE_NAME'],
            "POST_ID" => $this->arParams["POST_ID"],
            "PAGEN" => $this->arResult["PAGEN"],
            'CONTANER' => '.' . $this->arResult['WRAPPER']['CONTANER'],
            "CONTANER_LIST" => '.' . $this->arResult['WRAPPER']["CONTANER_LIST"],
            "CONTANER_MORE" => '.' . $this->arResult['WRAPPER']["CONTANER_MORE"],
        );
        ?>
        <script>
            var <?= $this->arResult['WRAPPER']['JS_OBJECT'] ?> = new jsProjectCommentBlogComment(<?= CUtil::PhpToJSObject($jsParams, false, true) ?>);
        </script>
        <?
    }

    private function postCommentSort(&$list, $path, $parent = 0, $level = 0) {
        $level++;
        if (isset($path[$parent])) {
            foreach ($path[$parent] as $value) {
                if (isset($path[$value])) {
                    self::postCommentSort($list, $path, $value, $level);
                }
                $list[$value] = array(
                    'ID' => $value,
                    'LEVEL' => $level,
                );
            }
        }
    }

    public function executeComponent() {
        $this->arParams['POST_ID'] = (int) $this->arParams['POST_ID'];
        if (Loader::includeModule('project.comment') and CUser::IsAuthorized() and $this->arParams['POST_ID']) {
            $this->arResult['IS_AJAX'] = isset($this->arParams['IS_AJAX']) and $this->arParams['IS_AJAX'] == 'Y';
            $this->arResult['IS_FILL'] = isset($this->arParams['IS_UPDATE']) ? (int) $this->arParams['IS_UPDATE'] : 1;
            $this->arResult['PAGEN'] = $this->arParams['PAGEN'] ?: 1;
            $this->arResult['LIMIT'] = 5;
            if ($this->arResult['IS_AJAX'] and $this->arParams['IS_UPDATE']) {
                $this->arResult['LIMIT'] *= $this->arParams['PAGEN'];
                $this->arResult['PAGEN'] = 1;
            }

            $arOrder = Array("ID" => "DESC");
            $arFilter = Array(
                "BLOG_ID" => Project\Comment\Config::BLOG_ID,
                "POST_ID" => $this->arParams["POST_ID"],
                "PUBLISH_STATUS" => 'P'
            );
            $arSelectedFields = Array(
                "ID",
                "PARENT_ID",
            );
            $dbComment = CBlogComment::GetList($arOrder, $arFilter, false, false, $arSelectedFields);
            $this->arResult['PATH'] = $this->arResult['MESSAGE'] = array();
            while ($arComment = $dbComment->GetNext()) {
                $this->arResult['PATH'][(int) $arComment["PARENT_ID"]][$arComment["ID"]] = $arComment["ID"];
            }
            self::postCommentSort($this->arResult['MESSAGE'], $this->arResult['PATH']);
            unset($this->arResult['PATH']);
            if ($this->arResult['MESSAGE']) {
                $arSelectedFields = Array(
                    "ID",
                    "PARENT_ID",
                    "AUTHOR_ID",
                    "POST_TEXT",
                    "DATE_CREATE",
                );
                $arFilter['ID'] = array_keys($this->arResult['MESSAGE']);
                $arParams = array(
                    'DATE_TIME_FORMAT' => 'H:i d.m.Y',
                    'PATH_TO_SMILE' => '/bitrix/images/blog/smile/',
                    'IMAGE_MAX_WIDTH' => 200,
                    'IMAGE_MAX_HEIGHT' => 200
                );
                $p = new blogTextParser(false, $arParams["PATH_TO_SMILE"]);
                $arParserParams = Array(
                    "imageWidth" => $arParams["IMAGE_MAX_WIDTH"],
                    "imageHeight" => $arParams["IMAGE_MAX_HEIGHT"],
                );
                $arNavStartParams = array(
                    "bDescPageNumbering" => false,
                    "nPageSize" => $this->arResult["LIMIT"],
                    "bShowAll" => false,
                    "iNumPage" => $this->arResult['PAGEN']
                );

                $dbComment = CBlogComment::GetList($arOrder, $arFilter, false, $arNavStartParams, $arSelectedFields);
                while ($arComment = $dbComment->GetNext()) {
                    $this->arResult['MESSAGE'][$arComment["ID"]]['AUTHOR'] = Igromafia\Game\User::getById($arComment["AUTHOR_ID"]);
                    $this->arResult['MESSAGE'][$arComment["ID"]]['POST_MESSAGE_TEXT'] = $p->convert($arComment["~POST_TEXT"], false, $arImages, $arAllow, $arParserParams);
                    $this->arResult['MESSAGE'][$arComment["ID"]]['DATE'] = FormatDate($arParams["DATE_TIME_FORMAT"], MakeTimeStamp($arComment["DATE_CREATE"], CSite::GetDateFormat("FULL")));
                }

                foreach ($this->arResult['MESSAGE'] as $key => $value) {
                    if(empty($value['POST_MESSAGE_TEXT'])) {
                        unset($this->arResult['MESSAGE'][$key]);
                    }
                }

                $this->arResult['PAGE_COUNT'] = $dbComment->NavPageCount;
                $this->arResult['PAGE_ITEM'] = $dbComment->NavPageNomer;
                $this->arResult['PAGE_IS_NEXT'] = $this->arResult['PAGE_ITEM'] < $this->arResult['PAGE_COUNT'];

                $this->arResult['POST'] = $dbComment->nSelectedCount;
            }

            $this->arResult['TEMPLATE_NAME'] = $this->GetTemplateName();
            $key = sha1($this->arResult['TEMPLATE_NAME'] . $this->arParams['POST_ID'] ?: '');

            $this->arResult['WRAPPER'] = array(
                'CONTANER' => 'ajax-personal-comment-blog-comment' . $key,
                'CONTANER_LIST' => 'ajax-personal-comment-blog-comment-list' . $key,
                'CONTANER_MORE' => 'ajax-personal-comment-blog-comment-more' . $key,
                'JS_OBJECT' => 'jsProjectCommentBlogComment' . $key,
            );
            if (empty($this->arResult['IS_AJAX'])) {
                $this->startWrapperTemplate();
            }
            $this->includeComponentTemplate();
            return $this->arResult['PAGE_IS_NEXT'] ?: false;
        }
    }

}
