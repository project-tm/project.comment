<?php

use Bitrix\Main\Loader,
    Bitrix\Main\Page\Asset,
    Bitrix\Main\Application,
    Project\Comment\Post;

class ProjectCommentPersonalList extends CBitrixComponent {

    public function executeComponent() {
        if (Loader::includeModule('project.comment') and CUser::IsAuthorized()) {
            $this->arResult['FILTER'] = Post::getList();
            $this->includeComponentTemplate();
        }
    }

}
