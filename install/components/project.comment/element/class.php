<?php

use Bitrix\Main\Loader,
    Bitrix\Main\Page\Asset,
    Bitrix\Main\Application,
    Project\Comment\Post;

class ProjectCommentElement extends CBitrixComponent {

    private function startWrapperTemplate() {
        $this->arResult['AJAX'] = $this->GetPath() . '/ajax.php';
        $this->arResult['SCRIPT'] = $this->GetPath() . '/script.js';
        Asset::getInstance()->addJs($this->arResult['SCRIPT']);
        $jsParams = array(
            'AJAX' => $this->arResult['AJAX'],
            'TEMPLATE_NAME' => $this->arResult['TEMPLATE_NAME'],
            "POST_ID" => $this->arResult["POST_ID"],
            "PAGEN" => $this->arResult["PAGEN"],
            'CONTANER' => '.' . $this->arResult['WPAPPER']['CONTANER'],
            'CONTANER_DELETE' => '.' . $this->arResult['WPAPPER']['CONTANER_DELETE'],
        );
        ?>
        <script>
            var <?= $this->arResult['JS_OBJECT'] ?> = new jsProjectCommentElement(<?= CUtil::PhpToJSObject($jsParams, false, true) ?>);
        </script>
        <?
    }

    public function executeComponent() {
        $this->arParams['POST_ID'] = (int)($this->arParams['POST_ID'] ?: 0);
        if (Loader::includeModule('project.comment') and CUser::IsAuthorized() and $this->arParams['POST_ID']) {
            $this->arResult['IS_AJAX'] = isset($this->arParams['IS_AJAX']) and $this->arParams['IS_AJAX'] == 'Y';
            $this->arResult['IS_FILL'] = isset($this->arParams['IS_UPDATE']) ? (int) $this->arParams['IS_UPDATE'] : 1;
            $this->arResult['PAGEN'] = $this->arParams['PAGEN'] ?: 1;
            $this->arResult['LIMIT'] = 10;
            if ($this->arResult['IS_AJAX'] and $this->arParams['IS_UPDATE']) {
                $this->arResult['LIMIT'] *= $this->arParams['PAGEN'];
                $this->arResult['PAGE'] = 1;
            }

            $arOrder = Array("DATE_CREATE" => "DESC", "ID" => "ASC");
            $arFilter = Array(
                "BLOG_ID" => Project\Comment\Config::BLOG_ID,
                "POST_ID" => $this->arParams["POST_ID"],
                "PUBLISH_STATUS" => 'P'
            );
            $arNavStartParams = array(
                "bDescPageNumbering" => false,
                "nPageSize" => $this->arResult["LIMIT"],
                "bShowAll" => false,
                "iNumPage" => $this->arResult['PAGEN']
            );
            $arSelectedFields = Array(
                "AUTHOR_ID",
                "POST_TEXT",
                "DATE_CREATE",
            );
//            pre($arNavStartParams);
            $dbComment = CBlogComment::GetList($arOrder, $arFilter, false, $arNavStartParams, $arSelectedFields);
            $this->arResult['MESSAGE'] = array();
            while ($arComment = $dbComment->GetNext()) {
                $this->arResult['MESSAGE'][] = array(
                    'ID' => $arComment["ID"],
                    'USER' => Igromafia\Game\User::getById($arComment["AUTHOR_ID"]),
                    'TextFormated' => $p->convert($arComment["~POST_TEXT"], false, $arImages, $arAllow, $arParserParams),
                    'DateFormated' => FormatDate($arParams["DATE_TIME_FORMAT"], MakeTimeStamp($arComment["DATE_CREATE"], CSite::GetDateFormat("FULL"))),
                );
            }
            pre($this->arResult['MESSAGE']);

            $this->arResult['PAGE_COUNT'] = $res->NavPageCount;
            $this->arResult['PAGE_ITEM'] = $res->NavPageNomer;
            $this->arResult['PAGE_IS_NEXT'] = $this->arResult['PAGE_ITEM'] < $this->arResult['PAGE_COUNT'];

            $this->arResult['POST'] = $res->nSelectedCount;

            $request = Application::getInstance()->getContext()->getRequest();

            $this->arResult['PARAM'] = $this->arParams['PARAM'] ?: '';
            $this->arResult['TEMPLATE_NAME'] = $this->GetTemplateName();
            $key = sha1($this->arResult['TEMPLATE_NAME'] . $this->arResult['PARAM'] ?: '');

            $this->arResult['WPAPPER'] = array(
                'CONTANER' => 'ajax-personal-comment-element' . $key,
                'JS_OBJECT' => 'jsProjectCommentElement' . $key,
            );
            if (empty($this->arResult['IS_AJAX'])) {
                $this->startWrapperTemplate();
            }
            $this->includeComponentTemplate();
            return true;
        }
    }

}
